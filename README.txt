Installation
============

Extract this tarball in your sites/all/modules or some other modules
folder.

Go to Administer -> User -> Permissions and make your choices for
the PAD File module. You have access, create, edit and administration
permissions.

Go to Administer -> Content Management -> PAD File to create your
new PAD Files.


Uninstalling
============

This module manages several tables in your database. To completely
uninstall this module, please follow this procedure:

Go to the module administration panel, unselect PAD File, click Save.
Go to Uninstall, select PAD File and click on Delete. Then confirm.
At that point all the data saved in the Database and on disk will
have been removed.


Management
==========

Go to Administer -> Content Management -> PAD File and click on edit
of the PAD File you want to modify.

This gives you access to the main settings. Click on Descriptions list
or Add description to manage the program descriptions.

Use the Tools to update all the PAD files at once (you can delete,
activate/desactive, and reset the download counter on all the PAD files
at once.)

To change the minimum expected size of one of your product file, click
on PAD File Settings and change the size there. Any PAD file created
or edited after that will need to be at least equal to that size.


Security Issues
===============

It is possible to let any user create and edit PAD files. However, at
this time, they immediately are active.

If the need arise, we may develop a way to let only a set of roles manage
the Active flag.


