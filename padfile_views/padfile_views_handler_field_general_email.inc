<?php

/**
 * @file
 * Handle the General Email field.
 */

class padfile_views_handler_field_general_email extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['general_email'] = 'general_email';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Mail link to contact company or author');
  }

  function render($values) {
    return $this->render_link(t('General contact email'), $values,
                            'mailto:' . $values->{$this->aliases['general_email']});
  }

};

