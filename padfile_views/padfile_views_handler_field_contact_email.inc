<?php

/**
 * @file
 * Handle the Contact Email field.
 */

class padfile_views_handler_field_contact_email extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['contact_email'] = 'contact_email';
    $this->additional_fields['contact_first_name'] = 'contact_first_name';
    $this->additional_fields['contact_last_name'] = 'contact_last_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Mail link to contact');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['contact_first_name']} . ' '
                            . $values->{$this->aliases['contact_last_name']}, $values,
                            'mailto:' . $values->{$this->aliases['contact_email']});
  }

};

