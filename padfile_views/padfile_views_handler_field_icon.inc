<?php

/**
 * @file
 * Handle the Icon field.
 */

class padfile_views_handler_field_icon extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['icon_url'] = 'icon_url';
    $this->additional_fields['download_url_primary'] = 'download_url_primary';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['image_width'] = array('default' => 0);
    $options['image_height'] = array('default' => 0);
    $options['image_border'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Link icon to download');
    $form['image_width'] = array(
      '#title' => t('Width used to display the icon'),
      '#type' => 'textfield',
      '#default_value' => $this->options['image_width'],
    );
    $form['image_height'] = array(
      '#title' => t('Height used to display the icon'),
      '#type' => 'textfield',
      '#default_value' => $this->options['image_height'],
    );
    $form['image_border'] = array(
      '#title' => t('Display a border around the image when checked'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['image_border']),
    );
    unset($form['link_to_label']);
  }

  function render($values) {
    $attr = '';
    if ($this->options['image_width'] > 0) {
      $attr .= ' width="' . $this->options['image_width'] . '"';
    }
    if ($this->options['image_height'] > 0) {
      $attr .= ' height="' . $this->options['image_height'] . '"';
    }
    $attr .= ' border="' . $this->options['image_border'] . '"';
    $label = '<img src="' . $values->{$this->field_alias} . '"' . $attr . '/>';
    $link = $values->{$this->aliases['download_url_primary']};
    if ($link) {
      return $this->render_link($label, $values, $link);
    }
    return $label;
  }

};

