<?php

/**
 * @file
 * Handle the Author field.
 */

class padfile_views_handler_field_author_email extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['author_email'] = 'author_email';
    $this->additional_fields['author_first_name'] = 'author_first_name';
    $this->additional_fields['author_last_name'] = 'author_last_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Mail link to author');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['author_first_name']} . ' '
                            . $values->{$this->aliases['author_last_name']}, $values,
                            'mailto:' . $values->{$this->aliases['author_email']});
  }

};

