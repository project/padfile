<?php

/**
 * @file
 * Handle the Download URL other #2 field.
 */

class padfile_views_handler_field_download_url_other2 extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['download_url_other1'] = 'download_url_other2';
    $this->additional_fields['program_name'] = 'program_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Backup link #3 to program');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['program_name']}, $values,
                              $values->{$this->aliases['download_url_other2']});
  }

};

