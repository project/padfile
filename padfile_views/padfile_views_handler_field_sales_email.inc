<?php

/**
 * @file
 * Handle the Sales field.
 */

class padfile_views_handler_field_sales_email extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['sales_email'] = 'sales_email';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Mail link to sales department');
  }

  function render($values) {
    return $this->render_link(t('Sales email'), $values,
                            'mailto:' . $values->{$this->aliases['sales_email']});
  }

};

