<?php

/**
 * @file
 * Handle the Download URL Primary field.
 */

class padfile_views_handler_field_download_url_primary extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['download_url_primary'] = 'download_url_primary';
    $this->additional_fields['program_name'] = 'program_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Link to program download');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['program_name']}, $values,
                              $values->{$this->aliases['download_url_primary']});
  }

};

