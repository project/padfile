<?php

/**
 * @file
 * Handle the Info field.
 */

class padfile_views_handler_field_info extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['info_url'] = 'info_url';
    $this->additional_fields['program_name'] = 'program_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Link to program detailed web page');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['program_name']}, $values,
                              $values->{$this->aliases['info_url']});
  }

};

