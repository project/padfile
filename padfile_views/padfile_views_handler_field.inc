<?php

/**
 * @file
 * PAD File view field basic handler.
 */

class padfile_views_handler_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['padid'] = 'padid';
    $this->additional_fields['padfile_name'] = 'padfile_name';
    $this->additional_fields['active'] = 'active';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_padfile'] = array('default' => FALSE);
    $options['link_to_blank'] = array('default' => TRUE);
    $options['link_to_label'] = array('default' => "");
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile'] = array(
      '#title' => t('Link this field to its PAD File'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_padfile']),
    );
    $form['link_to_blank'] = array(
      '#title' => t('Make the link open a new window'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_blank']),
    );
    $form['link_to_label'] = array(
      '#title' => t('Link label'),
      '#description' => t('If not empty, use this as the link label instead of the default.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['link_to_label'],
    );
  }

  function render_link($data, $values, $link = NULL) {
    // if not active, we do not want a link
    if ($values->{$this->aliases['active']} && !empty($this->options['link_to_padfile']) && $data !== NULL && $data !== '') {
      $label = $data;
      if ($this->options['link_to_label']) {
        // user label instead (note that for images we do not let users define the label so
        // it simply never goes here and uses the image instead.)
        $label = $this->options['link_to_label'];
      }
      $opt = array();
      if ($this->options['link_to_blank']) {
        $opt += array('target' => '_blank');
      }
      if (count($opt) > 0) {
        $args = array('attributes' => $opt);
      }
      if (!$link) {
        $link = "padfile/" . $values->{$this->aliases['padfile_name']} . ".xml";
      }
      return l($label, $link, array('html' => TRUE) + $args);
    }
    return $data;
  }

  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }

};


