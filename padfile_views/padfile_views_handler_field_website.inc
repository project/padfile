<?php

/**
 * @file
 * Handle the Website field.
 */

class padfile_views_handler_field_website extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['website'] = 'website';
    $this->additional_fields['company_name'] = 'company_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Link to company or author web page');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['company_name']}, $values,
                              $values->{$this->aliases['website']});
  }

};

