<?php

/**
 * @file
 * Handle the Ordering field.
 */

class padfile_views_handler_field_ordering extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['ordering_url'] = 'ordering_url';
    $this->additional_fields['program_name'] = 'program_name';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Link to the page where one can pay for the program');
  }

  function render($values) {
    return $this->render_link($values->{$this->aliases['program_name']}, $values,
                              $values->{$this->aliases['ordering_url']});
  }

};

