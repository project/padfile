<?php

/**
 * @file
 * Handle the Screenshot field.
 */

class padfile_views_handler_field_screenshot extends padfile_views_handler_field_icon {
  function construct() {
    parent::construct();
    $this->additional_fields['screenshot_url'] = 'screenshot_url';
  }

};

