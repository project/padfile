<?php

/**
 * @file
 * Handle the Support Email field.
 */

class padfile_views_handler_field_support_email extends padfile_views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['support_email'] = 'support_email';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_padfile']['#title'] = t('Mail link to support department');
  }

  function render($values) {
    return $this->render_link(t('Support email'), $values,
                            'mailto:' . $values->{$this->aliases['support_email']});
  }

};

