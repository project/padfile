<?php

/**
 * @file
 * Handle the Active field.
 */

class padfile_views_handler_field_active extends padfile_views_handler_field {
  function construct() {
    parent::construct();
  }

  function render($values) {
    return $this->render_link($values->{$this->field_alias} ? t('Active') : t('Off'), $values);
  }
};
