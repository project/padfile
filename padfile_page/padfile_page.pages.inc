<?php

/**
 * @file
 * Implementation of the page output which is to theme the exisitng PAD File.
 */

/**
 * Menu callback
 */
function padfile_page_output($padfile_name) {
  $result = db_query("SELECT * FROM {padfile} WHERE padfile_name = '%s' AND active = 1", $padfile_name);
  $padfile = db_fetch_array($result);
  if (!$padfile) {
    drupal_set_message(t('PAD File @name not found.', array('@name' => $padfile_name)), 'error');
    drupal_not_found();
    return;
  }

  // we need to know where we can get the language from (we want to use the browser language)
  $result = db_query("SELECT * FROM {padfile_descriptions} WHERE padid = %d AND language = '%s'", $padfile['padid'], 'English');
  $padfile_description = db_fetch_array($result);
  if (!$padfile_description) {
    drupal_set_message(t('PAD File description for @name not found.', array('@name' => $padfile_name)), 'error');
    drupal_not_found();
    return;
  }

  // copy inside the padfile since we want only one language and fields are unique among both tables
  foreach ($padfile_description as $key => $value) {
    $padfile[$key] = $value;
  }

  drupal_set_title($padfile['program_name']);

  drupal_add_link(array('rel' => 'padfile',
                        'type' => 'application/xml-padfile',
                        'title' => $padfile['program_name'],
                        'href' => url('padfile/' . $padfile['padfile_name'] . '.xml', array('absolute' => TRUE))));

  return theme('padfile_page', $padfile);
}
