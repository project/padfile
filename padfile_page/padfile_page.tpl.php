<div id="padfile_page">
  <?php
    // Show all the fields
    //foreach ($padfile as $key => $value) {
    //  echo "<div>", $key, ": ", $value, "</div>";
    //}
    echo '<img src="', $padfile['icon_url'], '" align="left" style="padding-right: 10px; padding-bottom: 10px;"/>';
    echo '<h3>Created by <a href="', $padfile['website'], '">', $padfile['company_name'], '</a></h3>';
    echo '<p>', $padfile['desc450'], '</p>';
    if ($padfile['distribution_permissions']) {
      echo '<p style="border: 1px solid gray; margin: 20px 5px; padding: 5px; background-color: #f0f0f0; colod: #444444;">',
        $padfile['distribution_permissions'], '</p>';
    }
    echo '<p style="text-align: right;">';
    if (($_GLOBAL['user']->uid == $uid && user_access('edit own padfile'))
        || user_access('edit any padfile')) {
      echo l(t('Edit PAD File'), 'admin/content/padfile/edit/' . $padfile['padid']), ' &nbsp; &nbsp; ';
    }
    echo '<a href="/padfile/', $padfile['padfile_name'], '.xml">PAD File</a>',
      ' &nbsp; &nbsp; <a href="', $padfile['download_url_primary'], '">Download Now!</a>',
      ' &nbsp; &nbsp; <a href="', $padfile['info_url'], '">More Info</a></p>';
  ?>
</div>
