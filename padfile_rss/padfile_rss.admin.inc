<?php

/**
 * @file
 * Implementation of the PAD File RSS forms.
 */

/**
 * Implementation of the form altering to add the RSS fields.
 *
 * We place the feed URLs along the Descriptions/Pictures since
 * it somewhat corresponds (i.e. they are URLs.)
 *
 * \param[in,out] $form The form being altered.
 * \param[in] $form_state The form current state.
 */
function padfile_rss_padfile_form(&$form, $form_state) {
  if (isset($form['padid']['#value'])) {
    $sql = "SELECT company_rss, product_rss FROM {padfile_rss} WHERE padid = %d";
    $result = db_query($sql, $form['padid']['#value']);
    $edit = db_fetch_array($result);
  }
  else {
    // by default we use empty strings at this time, we could also look into
    // determining defaults (i.e. <this site>/rss.xml for the company RSS.)
    $edit = array(
      'company_rss' => '',
      'product_rss' => '',
    );
  }
  $form['urls']['company_rss'] = array(
    '#type' => 'textfield',
    '#maxlength' => 130,
    '#title' => t('Company RSS URL'),
    '#default_value' => $edit['company_rss'],
    '#description' => t('This URL must point to an RSS feed of the company providing this product.'),
  );
  $form['urls']['product_rss'] = array(
    '#type' => 'textfield',
    '#maxlength' => 130,
    '#title' => t('Product RSS URL'),
    '#default_value' => $edit['product_rss'],
    '#description' => t('This URL must point to an RSS feed talking about this and related product.'),
  );

  $form['#submit'][] = 'padfile_rss_padfile_form_submit';
}

/**
 * Implementation of the submit function of the RSS padfile form.
 *
 * \param[in] $form The form being submitted.
 * \param[in,out] $form_state The current status of the form.
 */
function padfile_rss_padfile_form_submit($form, &$form_state) {
  $padid = $form_state['values']['padid'];
  $sql = "UPDATE {padfile_rss} SET company_rss = '%s', product_rss = '%s' WHERE padid = %d";
  db_query($sql, $form_state['values']['company_rss'], $form_state['values']['product_rss'], $padid);
  if (db_affected_rows() <= 0) {
    $sql = "INSERT INTO {padfile_rss} (padid, company_rss, product_rss) VALUES (%d, '%s', '%s')";
    // the @ is for broken MySQL implementations
    @db_query($sql, $padid, $form_state['values']['company_rss'], $form_state['values']['product_rss']);
  }
}

// vim: ts=2 sw=2 et syntax=php
