<?php

/**
 * @file
 * Implementation of the PAD File RSS output.
 */


/**
 * Output the RSS feed extension tag.
 *
 * This function checks whether there is a company and/or a product RSS feed.
 * If so, it adds the PAD RSS extension.
 *
 * \param[in] $pad The PAD object as defined in the padfile_output() function.
 */
function padfile_rss_output($pad) {
  // read the RSS PAD file
  $result = db_query("SELECT company_rss, product_rss FROM {padfile_rss} WHERE padid = %d", $pad->padid);
  $row = db_fetch_array($result);

  if (!empty($row)) {
    echo ' <RSS>', "\n";
    echo '  <RSS_FORM>Y</RSS_FORM>', "\n";
    echo '  <RSS_DESCRIPTION>RSS Feed Extension</RSS_DESCRIPTION>', "\n";
    echo '  <RSS_VERSION>1.0</RSS_VERSION>', "\n";
    echo '  <RSS_URL>http://pad.asp-software.org/extensions/RSS.htm</RSS_URL>', "\n";
    echo '  <RSS_SCOPE>Product</RSS_SCOPE>', "\n";
    if ($row['company_rss']) {
      echo '  <RSS_Feed_Company>', $row['company_rss'], '</RSS_Feed_Company>', "\n";
    }
    if ($row['product_rss']) {
      echo '  <RSS_Feed_Product>', $row['product_rss'], '</RSS_Feed_Product>', "\n";
    }
    echo ' </RSS>', "\n";
  }
}

// vim: ts=2 sw=2 et syntax=php
