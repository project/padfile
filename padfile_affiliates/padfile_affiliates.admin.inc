<?php

/**
 * @file
 * Implementation of the PAD File Affiliates forms.
 */

/**
 * Implementation of the form altering to add the Affiliates fields.
 *
 * We place the feed URLs along the Descriptions/Pictures since
 * it somewhat corresponds (i.e. they are URLs.)
 *
 * \param[in,out] $form The form being altered.
 * \param[in] $form_state The form current state.
 */
function padfile_affiliates_padfile_form(&$form, $form_state) {
  global $_padfile_affiliates_companies;

  module_load_include('inc', 'padfile_affiliates');

  if (isset($form['padid']['#value'])) {
    $sql = "SELECT affiliate_information_page, affiliate_companies FROM {padfile_affiliates} WHERE padid = %d";
    $result = db_query($sql, $form['padid']['#value']);
    $edit = db_fetch_array($result);
    if (isset($edit['affiliate_companies']) && is_string($edit['affiliate_companies'])) {
      $edit['affiliate_companies'] = unserialize($edit['affiliate_companies']);
    }
  }
  else {
    $edit = array(
      'affiliate_information_page' => '',
    );
  }
  // ensure that the affiliate companies entry is an array
  if (!isset($edit['affiliate_companies']) || !is_array($edit['affiliate_companies'])) {
    $edit['affiliate_companies'] = array();
  }
  // ensure that all the affiliate companies are well defined
  foreach ($_padfile_affiliates_companies as $c) {
    if (empty($edit['affiliate_companies'][$c])) {
      $edit['affiliate_companies'][$c] = array();
    }
    $edit['affiliate_companies'][$c] += array(
      'order_page' => '',
      'vendor_id' => '',
      'product_id' => '',
      'maximum_commission_rate' => '',
    );
  }

  $form['affiliates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Affiliates'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('URLs and identifiers of different affiliate programs you are using.'),
  );

  $form['affiliates']['affiliate_information_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate Information Page'),
    '#maxlength' => 130,
    '#default_value' => $edit['affiliate_information_page'],
  );

  foreach ($_padfile_affiliates_companies as $c) {
    $collapsed = empty($edit['affiliate_companies'][$c]['order_page'])
      && empty($edit['affiliate_companies'][$c]['vendor_id'])
      && empty($edit['affiliate_companies'][$c]['product_id'])
      && empty($edit['affiliate_companies'][$c]['maximum_commission_rate']);
    $form['affiliates'][$c] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($c),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
      '#description' => t('URLs and identifiers of different affiliate programs you are using.'),
    );
    $form['affiliates'][$c]['affiliate_' . $c . '_order_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Order Page URL'),
      '#maxlength' => 130,
      '#default_value' => $edit['affiliate_companies'][$c]['order_page'],
    );
    $form['affiliates'][$c]['affiliate_' . $c . '_vendor_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Your vendor identifier with this system'),
      '#maxlength' => 60,
      '#default_value' => $edit['affiliate_companies'][$c]['vendor_id'],
    );
    $form['affiliates'][$c]['affiliate_' . $c . '_product_id'] = array(
      '#type' => 'textfield',
      '#title' => t('This very product identifier'),
      '#maxlength' => 60,
      '#default_value' => $edit['affiliate_companies'][$c]['product_id'],
    );
    $form['affiliates'][$c]['affiliate_' . $c . '_maximum_commission_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum commission rate'),
      '#maxlength' => 60,
      '#default_value' => $edit['affiliate_companies'][$c]['maximum_commission_rate'],
    );
  }

  $form['#submit'][] = 'padfile_affiliates_padfile_form_submit';
}

/**
 * Implementation of the submit function of the RSS padfile form.
 *
 * \param[in] $form The form being submitted.
 * \param[in,out] $form_state The current status of the form.
 */
function padfile_affiliates_padfile_form_submit($form, &$form_state) {
  global $_padfile_affiliates_companies;

  module_load_include('inc', 'padfile_affiliates');

  $values = &$form_state['values'];

  $padid = $values['padid'];
  $affiliate_information_page = $values['affiliate_information_page'];
  $affiliate_companies = array();
  // only grab those fields that are not just completely empty
  foreach ($_padfile_affiliates_companies as $c) {
    if (!empty($values['affiliate_' . $c . '_order_page'])) {
      $affiliate_companies[$c]['order_page'] = $values['affiliate_' . $c . '_order_page'];
    }
    if (!empty($values['affiliate_' . $c . '_vendor_id'])) {
      $affiliate_companies[$c]['vendor_id'] = $values['affiliate_' . $c . '_vendor_id'];
    }
    if (!empty($values['affiliate_' . $c . '_product_id'])) {
      $affiliate_companies[$c]['product_id'] = $values['affiliate_' . $c . '_product_id'];
    }
    if (!empty($values['affiliate_' . $c . '_maximum_commission_rate'])) {
      $affiliate_companies[$c]['maximum_commission_rate'] = $values['affiliate_' . $c . '_maximum_commission_rate'];
    }
  }
  $affiliate_companies = serialize($affiliate_companies);

  $sql = "UPDATE {padfile_affiliates} SET affiliate_information_page = '%s', affiliate_companies = '%s' WHERE padid = %d";
  db_query($sql, $affiliate_information_page, $affiliate_companies, $padid);
  if (db_affected_rows() <= 0) {
    $sql = "INSERT INTO {padfile_affiliates} (padid, affiliate_information_page, affiliate_companies) VALUES (%d, '%s', '%s')";
    // the @ is for broken MySQL implementations
    db_query($sql, $padid, $affiliate_information_page, $affiliate_companies);
  }
}

// vim: ts=2 sw=2 et syntax=php
