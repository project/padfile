<?php

/**
 * @file
 * List of companies for the PAD Affiliates extension.
 */

global $_padfile_affiliates_companies;
$_padfile_affiliates_companies = array(
  'Avangate',
  'BMTMicro',
  'Cleverbridge',
  'clixGalore',
  'CommissionJunction',
  'DigiBuy',
  'DigitalCandle',
  'Emetrix',
  'eSellerate',
  'iPortis',
  'Kagi',
  'LinkShare',
  'NorthStarSol',
  'OneNetworkDirect',
  'Order1',
  'Osolis',
  'Plimus',
  'Regnet',
  'Regnow',
  'Regsoft',
  'ShareIt',
  'Shareasale',
  'SWReg',
  'V-Share',
  'VFree',
  'Yaskifo',
);

// vim: ts=2 sw=2 et syntax=php
