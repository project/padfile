<?php

/**
 * @file
 * Implementation of the PAD File Affiliates output.
 */


/**
 * Output the Affiliates extension tag.
 *
 * This function checks whether there is a company and/or a product Affiliates.
 * If so, it adds the PAD Affiliates extension.
 *
 * \param[in] $pad The PAD object as defined in the padfile_output() function.
 */
function padfile_affiliates_output($pad) {
  // get the affiliates for this PAD file
  $result = db_query("SELECT affiliate_information_page, affiliate_companies FROM {padfile_affiliates} WHERE padid = %d", $pad->padid);
  $row = db_fetch_array($result);

  if (!empty($row)) {
    echo ' <Affiliates>', "\n";
    echo '  <Affiliates_FORM>Y</Affiliates_FORM>', "\n";
    echo '  <Affiliates_DESCRIPTION>Affiliates Extension</Affiliates_DESCRIPTION>', "\n";
    echo '  <Affiliates_VERSION>1.4</Affiliates_VERSION>', "\n";
    echo '  <Affiliates_URL>http://pad.asp-software.org/extensions/Affiliates.htm</Affiliates_URL>', "\n";
    echo '  <Affiliates_SCOPE>Product</Affiliates_SCOPE>', "\n";
    if (!empty($row['affiliate_information_page'])) {
      echo '  <Affiliates_Information_Page>', check_plain($row['affiliate_information_page']), '</Affiliates_Information_Page>', "\n";
    }
    if (!empty($row['affiliate_companies'])) {
      $row['affiliate_companies'] = unserialize($row['affiliate_companies']);
    }
    foreach ($row['affiliate_companies'] as $c => $info) {
      if (!empty($info['order_page'])) {
        echo '  <Affiliates_', $c, '_Order_Page>', check_plain($info['order_page']), '</Affiliates_', $c, "_Order_Page>\n";
      }
      if (!empty($info['vendor_id'])) {
        echo '  <Affiliates_', $c, '_Vendor_ID>', check_plain($info['vendor_id']), '</Affiliates_', $c, "_Vendor_ID>\n";
      }
      if (!empty($info['product_id'])) {
        echo '  <Affiliates_', $c, '_Product_ID>', check_plain($info['product_id']), '</Affiliates_', $c, "_Product_ID>\n";
      }
      if (!empty($info['maximum_commission_rate'])) {
        echo '  <Affiliates_', $c, '_Maximum_Commission_Rate>', check_plain($info['maximum_commission_rate']), '</Affiliates_', $c, "_Maximum_Commission_Rate>\n";
      }
    }
    echo ' </Affiliates>';
  }
}

// vim: ts=2 sw=2 et syntax=php
