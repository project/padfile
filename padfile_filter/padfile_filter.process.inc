<?php

/**
 * @file padfile_filter.process.inc
 *
 * This file includes all the necessary functions to process the PAD File filters.
 */

function _padfile_load_descriptions($pad, $language, $field, $value) {
  $desc = NULL;
  if ($language) {
    $sql = "SELECT * FROM {padfile_descriptions} WHERE padid = %d AND language = '%s'";
    $result = db_query($sql, $pad->padid, $language);
    $descriptions = array();
    $desc = db_fetch_object($result);
  }
  if (!$desc) {
    drupal_set_message(t('Description for @padfile in @lang cannot be found', array('@padfile' => $pad->padfile_name, '@lang' => $language)), 'error');
    return '<span style="color: red;">[padfile: ' . $value . ']</span>';
  }
  return str_replace("\n", "<br/>", $desc->$field);
}


function _padfile_filter_process($text, $format) {
  if (preg_match_all("/\[padfile: *([^]]+)\]/i", $text, $match)) {
    foreach ($match[1] as $key => $value) {
      // extract the options
      $value = str_replace(array(';', ',', '/'), array(' ', ' ', ' '), $value);
      $args = explode(' ', $value);
      switch (count($args)) {
      case 1:
        $language = '';
        $padfile_name = 'pad_file';
        $field = $args[0];
        break;

      case 2:
        $language = '';
        $padfile_name = $args[0];
        $field = $args[1];
        break;

      case 3:
      case 4:   // extra params like width & height
      case 5:
        $language = $args[0];
        $padfile_name = $args[1];
        $field = $args[2];
        break;

      default:
        drupal_set_message(t('Invalid number of options for PAD File filter tag @tag.', array('@tag' => $match[0][0])), 'error');
        // show it in red
        $search[] = $match[0][$key];
        $replace[] = '<span style="color: red;">[padfile: ' . $value . ']</span>';
        continue;

      }
      // accept - instead of _
      $field = str_replace('-', '_', $field);
      // first check fields that do not require a PAD File name
      //
      // PLEASE, KEEP FIELDS IN ALPHABETICAL ORDER
      // (unless you are adding equivalents)
      $need_pad = FALSE;
      switch ($field) {
      case 'count':
        $result = db_query("SELECT COUNT(*) AS count FROM {padfile} WHERE active = 1");
        if ($result) {
          $count = db_fetch_object($result);
          if ($count) {
            $replace[] = $count->count;
          }
        }
        break;

      case 'list':
        $replace[] = l(t('PAD File List'), 'padfile/list.xml');
        break;

      case 'total_count':
        $result = db_query("SELECT COUNT(*) AS count FROM {padfile}");
        if ($result) {
          $count = db_fetch_object($result);
          if ($count) {
            $replace[] = $count->count;
          }
        }
        break;

      case 'total_downloads':
        $result = db_query("SELECT SUM(download_counter) AS count FROM {padfile}");
        if ($result) {
          $count = db_fetch_object($result);
          if ($count) {
            $replace[] = $count->count;
          }
        }
        break;

      default:
        $need_pad = TRUE;
        break;

      }
      if ($need_pad) {
        if (is_numeric($padfile_name)) {
          $pad = padfile_padfile_load($padid);
        }
        else {
          $pad = _padfile_load_by_name($padfile_name);
        }
        // simple fields, we do not need to use the switch() below that way
        static $plain_fields = array(
          'address_1' => 'address_1',
          'address_2' => 'address_2',
          'author_first_name' => 'author_first_name',
          'author_last_name' => 'author_last_name',
          'author_email_only' => 'author_email',
          'byte_size' => 'byte_size',
          'category' => 'category',
          'city_town' => 'city_town',
          'company_name' => 'company_name',
          'contact_first_name' => 'contact_first_name',
          'contact_last_name' => 'contact_last_name',
          'contact_email_only' => 'contact_email',
          'cost' => 'cost',
          'country' => 'country',
          'currency_3letters' => 'currency',
          'download_counter' => 'download_counter',
          'download_url_primary_only' => 'download_url_primary',
          'download_url_other1_only' => 'download_url_other1',
          'download_url_other2_only' => 'download_url_other2',
          'download_url_secondary_only' => 'download_url_secondary',
          'expiration_base' => 'expiration_base',
          'expiration_count' => 'expiration_count',
          'expiration_info' => 'expiration_info',
          'fax_phone' => 'fax_phone',
          'general_email_only' => 'general_email',
          'general_phone' => 'general_phone',
          'icon_url_only' => 'icon_url',
          'info_url_only' => 'info_url',
          'install_support' => 'install_support',
          'languages' => 'languages',
          'operating_systems' => 'operating_systems',
          'ordering_url_only' => 'ordering_url',
          'padfile_name' => 'padfile_name',
          'program_name' => 'program_name',
          'requirements' => 'requirements',
          'sales_email_only' => 'sales_email',
          'sales_phone' => 'sales_phone',
          'screenshot_url_only' => 'screenshot_url',
          'state' => 'state',
          'status' => 'status',
          'support_email_only' => 'support_email',
          'support_phone' => 'support_phone',
          'type' => 'type',
          'version' => 'version',
          'website_url' => 'website',
          'zip' => 'zip',
        );
        if (!empty($plain_fields[$field])) {
          $replace[] = check_plain($pad->$plain_fields[$field]);
        }
        else {
          // PLEASE, KEEP FIELDS IN ALPHABETICAL ORDER
          // (unless you are adding equivalents)

          // For images, we want to make use of args 3 & 4
          if (isset($args[3]) && is_numeric($args[3])) {
            $width = ' width="' . round($args[3]) . '"';
          }
          else {
            $width = '';
          }
          if (isset($args[4]) && is_numeric($args[4])) {
            $height = ' height="' . round($args[4]) . '"';
          }
          else {
            $height = '';
          }
          $icon = '<img src="' . $pad->icon_url . '" alt="' . t('@program icon', array('@program', $pad->program_name)) . '"' . $width . $height . '/>';
          $screenshot = '<img src="' . $pad->screenshot_url . '" alt="' . t('@program icon', array('@program', $pad->program_name)) . '"' . $width . $height . '/>';

          switch ($field) {
          case 'active':
            $replace[] = $pad->active ? t('active') : t('off');
            break;

          case 'author_email':
            $replace[] = l(
              t(
                '!last_name, !first_name &lt;!email&gt;',
                array(
                  '!last_name' => $pad->author_last_name,
                  '!first_name' => $pad->author_first_name,
                  '!email' => $pad->author_email,
                )
              ),
              'mailto:' . $pad->author_email
            );
            break;

          case 'category_detail':
            $replace[] = check_plain(preg_replace('/.*::/', '', $pad->category));
            break;

          case 'category_group':
            $replace[] = check_plain(preg_replace('/::.*/', '', $pad->category));
            break;

          case 'change_log':
            $replace[] = str_replace("\n", "<br />", $pad->change_log);
            break;

          case 'contact_email':
            $replace[] = l(
              t(
                '!last_name, @first_name &lt;!email&gt;',
                array(
                  '!last_name' => $pad->contact_last_name,
                  '!first_name' => $pad->contact_first_name,
                  '!email' => $pad->contact_email,
                )
              ),
              'mailto:' . $pad->contact_email
            );
            break;

          case 'creation_date':
            $replace[] = date(variable_get('padfile_filter_date_' . $format, 'd/m/Y'), $pad->created_on);
            break;

          case 'creation_time':
            $replace[] = date(variable_get('padfile_filter_time_' . $format, 'H:i:s'), $pad->created_on);
            break;

          case 'currency':
            require_once('./' . drupal_get_path('module', 'padfile') . '/padfile.currencies.inc');
            $currencies = _padfile_currencies_array();
            $replace[] = $currencies[$pad->currency];
            break;

          case 'desc45':
          case 'desc80':
          case 'desc250':
          case 'desc450':
          case 'desc2000':
          case 'keywords':
            $replace[] = _padfile_load_descriptions($pad, $language, $field, $value);
            break;

          case 'distribution_permissions':
            $replace[] = str_replace("\n", "<br />", check_plain($pad->distribution_permissions));
            break;

          case 'download_url_primary':
            $replace[] = l(t('Download !program now!', array('!program' => $pad->program_name)), $pad->download_url_primary);
            break;

          case 'download_url_primary_with_icon':
            $replace[] = l($icon, $pad->download_url_primary, array('html' => TRUE));
            break;

          case 'download_url_other1':
            $replace[] = l(t('Download !program now!', array('!program' => $pad->program_name)), $pad->download_url_other1);
            break;

          case 'download_url_other2':
            $replace[] = l(t('Download @program now!', array('@program' => $pad->program_name)), $pad->download_url_other2);
            break;

          case 'download_url_secondary':
            $replace[] = l(t('Download @program now!', array('@program' => $pad->program_name)), $pad->download_url_secondary);
            break;

          case 'eula':
            $replace[] = str_replace("\n", "<br />", $pad->eula);
            break;

          case 'expiration':
            if ($pad->expiration_base == 'Days') {
              $replace[] =  t('@count days', array('@count' => $pad->expiration_count));
            }
            elseif ($pad->expiration_base == 'Uses') {
              $replace[] =  t('@count uses', array('@count' => $pad->expiration_count));
            }
            else {
              $replace[] =  t('@count days or uses', array('@count' => $pad->expiration_count));
            }
            break;

          case 'expiration_date':
            if ($pad->expiration_date < 100) {
              $replace[] = t('no expiration date');
            }
            else {
              $replace[] = date(variable_get('padfile_filter_date_' . $format, 'd/m/Y'), $pad->expiration_date);
            }
            break;

          case 'expiration_time':
            if ($pad->expiration_date < 100) {
              $replace[] = t('no expiration time');
            }
            else {
              $replace[] = date(variable_get('padfile_filter_time_' . $format, 'H:i:s'), $pad->expiration_date);
            }
            break;

          case 'export':
            if (!$language) {
              $language = 'all';
            }
            $language .= '/';
            $link = 'padfile/export/' . $language . $padfile_name . '.xml';
            $replace[] = l($pad->program_name, $link);
            break;

          case 'general_email':
            $replace[] = l($pad->general_email, 'mailto:' . $pad->general_email);
            break;

          case 'hr_size':
            if ($pad->byte_size < 1024) {
              $replace[] = $pad->byte_size . ' bytes';
            }
            elseif ($pad->byte_size < 768 * 1024) {
              $replace[] = round($pad->byte_size / 1024, 2) . 'Kb';
            }
            else {
              $replace[] = round($pad->byte_size / 1048576, 2) . 'Mb';
            }
            break;

          case 'icon_url':
            $replace[] = $icon;
            break;

          case 'info_url':
            $replace[] = l(t('More info about !program', array('!program' => $pad->program_name)), $pad->info_url);
            break;

          case 'info_url_with_icon':
            $replace[] = l($icon, $pad->info_url, array('html' => TRUE));
            break;

          case 'kb_size':
            $replace[] = round($pad->byte_size / 1024, 2);
            break;

          case 'link':
            if ($language) {
              $language .= '/';
            }
            $link = 'padfile/' . $language . $padfile_name . '.xml';
            $replace[] = l($pad->program_name, $link);
            break;

          case 'link_url':
            if ($language) {
              $language .= '/';
            }
            $link = 'padfile/' . $language . $padfile_name . '.xml';
            $replace[] = check_plain($link);
            break;

          case 'link_with_icon':
            if ($language) {
              $language .= '/';
            }
            $link = 'padfile/' . $language . $padfile_name . '.xml';
            $replace[] = l($icon, $link, array('html' => TRUE));
            break;

          case 'link_with_screenshot':
            if ($language) {
              $language .= '/';
            }
            $link = 'padfile/' . $language . $padfile_name . '.xml';
            $replace[] = l($screenshot, $link, array('html' => TRUE));
            break;

          case 'mb_size':
            $replace[] = round($pad->byte_size / 1048576, 2);
            break;

          case 'modification_date':
            $replace[] = date(variable_get('padfile_filter_date_' . $format, 'd/m/Y'), $pad->modified_on);
            break;

          case 'modification_time':
            $replace[] = date(variable_get('padfile_filter_time_' . $format, 'H:i:s'), $pad->modified_on);
            break;

          case 'ordering_url':
            $replace[] = l(t('Click to order !program', array('!program' => $pad->program_name)), $pad->ordering_url);
            break;

          case 'ordering_url_with_icon':
            $replace[] = l($icon, $pad->ordering_url, array('html' => TRUE));
            break;

          case 'padid':
            $replace[] = $pad->padid;
            break;

          case 'release_date':
            $replace[] = date(variable_get('padfile_filter_date_' . $format, 'd/m/Y'), $pad->release_date);
            break;

          case 'release_time':
            $replace[] = date(variable_get('padfile_filter_time_' . $format, 'H:i:s'), $pad->release_date);
            break;

          case 'sales_email':
            $replace[] = l($pad->sales_email, 'mailto:' . $pad->sales_email);
            break;

          case 'screenshot_url':
            $replace[] = $screenshot;
            break;

          case 'support_email':
            $replace[] = l($pad->support_email, 'mailto:' . $pad->support_email);
            break;

          case 'uid':
            $replace[] = $pad->uid;
            break;

          case 'user_name':
            $result = db_query("SELECT name FROM {users} WHERE uid = %d", $pad->uid);
            $user_name = db_fetch_object($result);
            if ($user_name) {
              $replace[] = $user_name->name;
            }
            else {
              $replace[] = '&lt;user name not found&gt;';
            }
            break;

          case 'website':
            $replace[] = l($pad->company_name, $pad->website);
            break;

          default:
            drupal_set_message(t('Unknown field @field for the PAD File filter.', array('@field' => $field)), 'error');
            $replace[] = '<span style="color: red;">[padfile: ' . $value . ']</span>';
            break;

          }
        }
      }
      $search[] = $match[0][$key];
    }
    return str_replace($search, $replace, $text);
  }
 
  // no match, ignore
  return $text;
}

// vim: ts=2 sw=2 et syntax=php
