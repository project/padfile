<?php

/**
 * @file
 * Implementation of the PAD File output.
 */


function _padfile_get_padfiles() {
  // we want a list of all the active PAD Files
  $padfiles = array();
  $result = db_query("SELECT * FROM {padfile} WHERE active = 1 ORDER BY program_name");
  while ($row = db_fetch_object($result)) {
    $padfiles[] = $row;
  }
  return $padfiles;
}

function padfile_output($language, $filename, $export = FALSE) {
  global $base_url;
  static $mod_version;
  if (!$mod_version) {
    $mod_version = PADFILE_MODULE_VERSION;
  }

  if ($language == 'all') {
    switch ($filename) {
    case 'list.xml':
      if ($export) {
        drupal_set_header('Content-type: application/octet-stream;');
      }
      else {
        drupal_set_header('Content-type: text/xml; charset=utf-8');
      }
    
      echo '<?xml version="1.0"?'.'>', "\n";
      echo '<XML_DIZ_LIST>', "\n";
      echo ' <MASTER_PAD_VERSION_INFO>', "\n";
      echo '  <MASTER_PAD_VERSION>', check_plain(PADFILE_MASTER_VERSION), '</MASTER_PAD_VERSION>', "\n";
      echo '  <MASTER_PAD_EDITOR>Drupal PAD File (', $mod_version, ')</MASTER_PAD_EDITOR>', "\n";
      echo '  <MASTER_PAD_INFO>http://www.asp-shareware.org/pad/spec/spec.php</MASTER_PAD_INFO>', "\n";
      echo ' </MASTER_PAD_VERSION_INFO>', "\n";
      echo ' <Application_XML_Files>', "\n";

      $padfiles = _padfile_get_padfiles();
      foreach ($padfiles as $pad) {
        echo '   <Application_XML_File_URL>', check_plain($base_url . '/padfile/' . $pad->padfile_name), '.xml</Application_XML_File_URL>', "\n";
      }

      echo ' </Application_XML_Files>', "\n";
      echo '</XML_DIZ_LIST>', "\n";

      exit();

    case 'padmap.txt':
      if ($export) {
        drupal_set_header('Content-type: application/octet-stream;');
      }
      else {
        drupal_set_header('Content-type: text/plain; charset=utf-8');
      }
      $padfiles = _padfile_get_padfiles();
      foreach ($padfiles as $pad) {
        echo check_plain($base_url . '/padfile/' . $pad->padfile_name), ".xml\n";
      }
      exit();

    }
  }

  // this happens when people use padfile/export/<filename>.xml
  if ($language == 'export') {
    $language = 'all';
    $export = TRUE;
  }
  elseif (strlen($language) == 2) {
    require_once('./' . drupal_get_path('module', 'padfile') . '/padfile.lang.inc');
    $l_array = _padfile_languages();
    $l = $l_array[$language];
    if (!$l) {
      drupal_set_message(t('Language @lang is not supported.', array('@lang' => $language)), 'error');
      drupal_not_found();
      exit();
    }
    $language = $l;
  }

  $padfile_name = basename($filename, ".xml");

  $result = db_query("SELECT * FROM {padfile} WHERE padfile_name = '%s' AND active = 1", $padfile_name);
  $pad = db_fetch_object($result);
  if (!$pad) {
    drupal_set_message(t('PAD File @name in @language language not found@export',
      array('@name' => $padfilen_name, '@language' => $language, '@export' => ($export ? ' for export' : ''))), 'error');
    drupal_not_found(); // PAD File $padfile_name not found
    exit();
  }

  $sql = "SELECT * FROM {padfile_descriptions} WHERE padid = %d";
  if ($language != "all") {
    $sql .= " AND language = '%s'";
  }
  $result = db_query($sql, $pad->padid, $language);
  $descriptions = array();
  while ($d = db_fetch_object($result)) {
    $descriptions[] = $d;
  }
  // Is it a problem if the descriptions array is empty?

  // update the counter
  db_query("UPDATE {padfile} SET download_counter = download_counter + 1 WHERE padfile_name = '%s' AND active = 1", $padfile_name);

  if ($export) {
    drupal_set_header('Content-type: application/octet-stream;');
  }
  else {
    drupal_set_header('Content-type: text/xml; charset=utf-8');
  }

  echo '<?xml version="1.0"?'.'>', "\n";
  echo '<XML_DIZ_INFO>', "\n";
  echo ' <MASTER_PAD_VERSION_INFO>', "\n";
  echo '  <MASTER_PAD_VERSION>', check_plain(PADFILE_MASTER_VERSION), '</MASTER_PAD_VERSION>', "\n";
  echo '  <MASTER_PAD_EDITOR>Drupal PAD File (', $mod_version, ')</MASTER_PAD_EDITOR>', "\n";
  echo '  <MASTER_PAD_INFO>http://www.asp-shareware.org/pad/spec/spec.php</MASTER_PAD_INFO>', "\n";
  echo ' </MASTER_PAD_VERSION_INFO>', "\n";
  echo ' <Company_Info>', "\n";
  // required, no test necessary
  echo '  <Company_Name>', check_plain($pad->company_name), '</Company_Name>', "\n";
  if ($pad->address_1) {
    echo '  <Address_1>', check_plain($pad->address_1), '</Address_1>', "\n";
    if ($pad->address_2) {
      echo '  <Address_2>', check_plain($pad->address_2), '</Address_2>', "\n";
    }
  }
  if ($pad->city_town) {
    echo '  <City_Town>', check_plain($pad->city_town), '</City_Town>', "\n";
  }
  if ($pad->state) {
    echo '  <State_Province>', check_plain($pad->state), '</State_Province>', "\n";
  }
  if ($pad->zip) {
    echo '  <Zip_Postal_Code>', check_plain($pad->zip), '</Zip_Postal_Code>', "\n";
  }
  if ($pad->country) {
    echo '  <Country>', check_plain($pad->country), '</Country>', "\n";
  }
  if ($pad->website) {
    echo '  <Company_WebSite_URL>', $pad->website, '</Company_WebSite_URL>', "\n";
  }
  if ($pad->author_first_name || $pad->author_last_name || $pad->author_email
   || $pad->contact_first_name || $pad->contact_last_name || $pad->contact_email) {
    echo '  <Contact_Info>', "\n";
    if ($pad->author_first_name) {
      echo '   <Author_First_Name>', check_plain($pad->author_first_name), '</Author_First_Name>', "\n";
    }
    if ($pad->author_last_name) {
      echo '   <Author_Last_Name>', check_plain($pad->author_last_name), '</Author_Last_Name>', "\n";
    }
    if ($pad->author_email) {
      echo '   <Author_Email>', check_plain($pad->author_email), '</Author_Email>', "\n";
    }
    if ($pad->contact_first_name) {
      echo '   <Contact_First_Name>', check_plain($pad->contact_first_name), '</Contact_First_Name>', "\n";
    }
    if ($pad->contact_last_name) {
      echo '   <Contact_Last_Name>', check_plain($pad->contact_last_name), '</Contact_Last_Name>', "\n";
    }
    if ($pad->contact_email) {
      echo '   <Contact_Email>', check_plain($pad->contact_email), '</Contact_Email>', "\n";
    }
    echo '  </Contact_Info>', "\n";
  }
  if ($pad->sales_email || $pad->support_email || $pad->general_email
   || $pad->sales_phone || $pad->support_phone || $pad->general_phone || $pad->fax_phone) {
    echo '  <Support_Info>', "\n";
    if ($pad->sales_email) {
      echo '   <Sales_Email>', check_plain($pad->sales_email), '</Sales_Email>', "\n";
    }
    if ($pad->support_email) {
      echo '   <Support_Email>', check_plain($pad->support_email), '</Support_Email>', "\n";
    }
    if ($pad->general_email) {
      echo '   <General_Email>', check_plain($pad->general_email), '</General_Email>', "\n";
    }
    if ($pad->sales_phone) {
      echo '   <Sales_Phone>', check_plain($pad->sales_phone), '</Sales_Phone>', "\n";
    }
    if ($pad->support_phone) {
      echo '   <Support_Phone>', check_plain($pad->support_phone), '</Support_Phone>', "\n";
    }
    if ($pad->general_phone) {
      echo '   <General_Phone>', check_plain($pad->general_phone), '</General_Phone>', "\n";
    }
    if ($pad->fax_phone) {
      echo '   <Fax_Phone>', check_plain($pad->fax_phone), '</Fax_Phone>', "\n";
    }
    echo '  </Support_Info>', "\n";
  }
  echo ' </Company_Info>', "\n";
  echo ' <Program_Info>', "\n";
  // required, no test necessary
  echo '  <Program_Name>', check_plain($pad->program_name), '</Program_Name>', "\n";
  if ($pad->version) {
    echo '  <Program_Version>', check_plain($pad->version), '</Program_Version>', "\n";
  }
  if ($pad->release_date > 100) {
    echo '  <Program_Release_Month>', gmdate('m', $pad->release_date), '</Program_Release_Month>', "\n";
    echo '  <Program_Release_Day>', gmdate('d', $pad->release_date), '</Program_Release_Day>', "\n";
    echo '  <Program_Release_Year>', gmdate('Y', $pad->release_date), '</Program_Release_Year>', "\n";
  }
  if ($pad->cost) {
    $cost = sprintf("%.02f", $pad->cost);
    if ($pad->currency == 'USD') {
      echo '  <Program_Cost_Dollars>', $cost, '</Program_Cost_Dollars>', "\n";
    }
    else {
      echo '  <Program_Cost_Other_Code>', check_plain($pad->currency), '</Program_Cost_Other_Code>', "\n";
      echo '  <Program_Cost_Other>', $cost, '</Program_Cost_Other>', "\n";
    }
  }
  // 3 dropdowns, always defined
  echo '  <Program_Type>', check_plain($pad->type), '</Program_Type>', "\n";
  echo '  <Program_Release_Status>', check_plain($pad->status), '</Program_Release_Status>', "\n";
  echo '  <Program_Install_Support>', check_plain($pad->install_support), '</Program_Install_Support>', "\n";
  if ($pad->operating_systems) {
    echo '  <Program_OS_Support>', check_plain($pad->operating_systems), '</Program_OS_Support>', "\n";
  }
  if ($pad->languages) {
    echo '  <Program_Language>', check_plain($pad->languages), '</Program_Language>', "\n";
  }
  echo '  <File_Info>', "\n";
  echo '   <File_Size_Bytes>', $pad->byte_size, '</File_Size_Bytes>', "\n";
  $kb = round($pad->byte_size / 1024, 2);
  if ($kb) {
    echo '   <File_Size_K>', $kb, '</File_Size_K>', "\n";
  }
  $mb = round($pad->byte_size / 1048576, 2);
  if ($mb) {
    echo '   <File_Size_MB>', $mb, '</File_Size_MB>', "\n";
  }
  echo '  </File_Info>', "\n";
  echo '  <Expire_Info>', "\n";
  $has_expire_info = $pad->expiration_count || $pad->expiration_info || $pad->expiration_date > 100;
  echo '   <Has_Expire_Info>', ($has_expire_info ? 'Y' : 'N'), '</Has_Expire_Info>', "\n";
  if ($pad->expiration_count) {
    echo '   <Expire_Count>', check_plain($pad->expiration_count), '</Expire_Count>', "\n";
    // base is always defined (drop-down) but only required if we have a Count
    echo '   <Expire_Based_On>', check_plain($pad->expiration_base), '</Expire_Based_On>', "\n";
  }
  if ($pad->expiration_info) {
    echo '   <Expire_Other_Info>', check_plain($pad->expiration_info), '</Expire_Other_Info>', "\n";
  }
  if ($pad->expiration_date > 100) {
    echo '   <Expire_Month>', gmdate('m', $pad->expiration_date), '</Expire_Month>', "\n";
    echo '   <Expire_Day>', gmdate('d', $pad->expiration_date), '</Expire_Day>', "\n";
    echo '   <Expire_Year>', gmdate('Y', $pad->expiration_date), '</Expire_Year>', "\n";
  }
  echo '  </Expire_Info>', "\n";
  if ($pad->change_log) {
    echo '  <Program_Change_Info>', check_plain($pad->change_log), '</Program_Change_Info>', "\n";
  }
  if ($pad->category) {
    echo '  <Program_Category_Class>', check_plain($pad->category), '</Program_Category_Class>', "\n";
  }
  if ($pad->requirements) {
    echo '  <Program_System_Requirements>', check_plain($pad->requirements), '</Program_System_Requirements>', "\n";
  }
  echo ' </Program_Info>', "\n";
  if (count($descriptions) > 0) {
    echo ' <Program_Descriptions>', "\n";
    foreach ($descriptions as $d) {
      if ($d->keywords || $d->desc45 || $d->desc80 || $d->desc250 || $d->desc450 || $d->desc2000) {
        echo '  <', $d->language, '>', "\n";
        if ($d->keywords) {
          echo '   <Keywords>', check_plain($d->keywords), '</Keywords>', "\n";
        }
        if ($d->desc45) {
          echo '   <Char_Desc_45>', check_plain($d->desc45), '</Char_Desc_45>', "\n";
        }
        if ($d->desc80) {
          echo '   <Char_Desc_80>', check_plain($d->desc80), '</Char_Desc_80>', "\n";
        }
        if ($d->desc250) {
          echo '   <Char_Desc_250>', check_plain($d->desc250), '</Char_Desc_250>', "\n";
        }
        if ($d->desc450) {
          echo '   <Char_Desc_450>', check_plain($d->desc450), '</Char_Desc_450>', "\n";
        }
        if ($d->desc2000) {
          echo '   <Char_Desc_2000>', check_plain($d->desc2000), '</Char_Desc_2000>', "\n";
        }
        echo '  </', $d->language, '>', "\n";
      }
    }
    echo ' </Program_Descriptions>', "\n";
  }
  echo ' <Web_Info>', "\n";
  echo '  <Application_URLs>', "\n";
  if ($pad->info_url) {
    echo '   <Application_Info_URL>', check_plain($pad->info_url), '</Application_Info_URL>', "\n";
  }
  if ($pad->ordering_url) {
    echo '   <Application_Order_URL>', check_plain($pad->ordering_url), '</Application_Order_URL>', "\n";
  }
  if ($pad->screenshot_url) {
    echo '   <Application_Screenshot_URL>', check_plain($pad->screenshot_url), '</Application_Screenshot_URL>', "\n";
  }
  if ($pad->icon_url) {
    echo '   <Application_Icon_URL>', check_plain($pad->icon_url), '</Application_Icon_URL>', "\n";
  }
  // required, no test necessary
  echo '   <Application_XML_File_URL>', check_plain($base_url . '/padfile/' . $pad->padfile_name), '.xml</Application_XML_File_URL>', "\n";
  echo '  </Application_URLs>', "\n";
  echo '  <Download_URLs>', "\n";
  // required, no test necessary
  echo '   <Primary_Download_URL>', check_plain($pad->download_url_primary), '</Primary_Download_URL>', "\n";
  // Do not follow user's order here...
  // Define secondary, other1 then other2 depending on available data
  if ($pad->download_url_secondary) {
    $other[] = $pad->download_url_secondary;
  }
  if ($pad->download_url_other1) {
    $other[] = $pad->download_url_other1;
  }
  if ($pad->download_url_other2) {
    $other[] = $pad->download_url_other2;
  }
  if ($other[0]) {
    echo '   <Secondary_Download_URL>', check_plain($other[0]), '</Secondary_Download_URL>', "\n";
  }
  if ($other[1]) {
    echo '   <Additional_Download_URL_1>', check_plain($other[1]), '</Additional_Download_URL_1>', "\n";
  }
  if ($other[2]) {
    echo '   <Additional_Download_URL_2>', check_plain($other[2]), '</Additional_Download_URL_2>', "\n";
  }
  echo '  </Download_URLs>', "\n";
  echo ' </Web_Info>', "\n";
  if ($pad->distribution_permissions || $pad->eula) {
    echo ' <Permissions>', "\n";
    if ($pad->distribution_permissions) {
      echo '   <Distribution_Permissions>', check_plain($pad->distribution_permissions), '</Distribution_Permissions>', "\n";
    }
    if ($pad->eula) {
      echo '   <EULA>', check_plain($pad->eula), '</EULA>', "\n";
    }
    echo ' </Permissions>', "\n";
  }
  // add the PADmap by default, user may turn it off
  if (variable_get('padfile_include_padmap', 1)) {
    echo ' <PADmap>', "\n";
    echo '  <PADmap_FORM>Y</PADmap_FORM>', "\n";
    echo '  <PADmap_DESCRIPTION>Link to plain text file containing all your PAD URLs from current host</PADmap_DESCRIPTION>', "\n";
    echo '  <PADmap_VERSION>1.0</PADmap_VERSION>', "\n";
    echo '  <PADmap_URL>http://www.padmaps.org/padmap.htm</PADmap_URL>', "\n";
    echo '  <PADmap_SCOPE>', check_plain($pad->company_name), '</PADmap_SCOPE>', "\n";
    echo '  <PADmap_Location>', check_plain($base_url), '/padfile/padmap.txt</PADmap_Location>', "\n";
    echo ' </PADmap>', "\n";
  }
  // add the PADRING only if the user turned it on
  if (variable_get('padfile_include_padring', 0)) {
    $padfiles = _padfile_get_padfiles();
    if (count($padfiles) > 1) {
      echo ' <PADRING>', "\n";
      foreach ($padfiles as $p) {
        // do not include ourself
        if ($p->padfile_name != $pad->padfile_name) {
          echo '  ', check_plain($base_url . '/padfile/' . $p->padfile_name), ".xml\n";
        }
      }
      echo ' </PADRING>', "\n";
    }
  }

  module_invoke_all('padfile_extension', 'padfile', $pad);

  // end the XML file
  echo '</XML_DIZ_INFO>', "\n";

  exit();
}

// vim: ts=2 sw=2 et syntax=php
